# A Line Bot using TS

請先在`.evn`中輸入Line Bot的Access Token、Channel Secret、User ID

## Project setup
```
$ yarn install
```
## Compiles and hot-reloads for development
```
$ yarn dev
```
## Compiles and minifies for production
```
$ yarn build
```

## Lints
```
$ yarn lint
```

## Deploy Using Docker compose
```
$ docker-compose up -d
```