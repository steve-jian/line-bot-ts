import dotenv from 'dotenv-flow';
import Server from './server';
import Bot from './bot';

dotenv.config();
Server.init();
Bot.init();

Server.getInstance().start();

Bot.getInstance().test();
